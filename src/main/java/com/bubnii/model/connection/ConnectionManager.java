package com.bubnii.model.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ConnectionManager {

    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("config");
    private static final String url = resourceBundle.getString("db.url");
    private static final String user = resourceBundle.getString("db.user");
    private static final String password = resourceBundle.getString("db.password");

    private static Connection connection = null;

    private ConnectionManager() {
    }

    public static Connection getConnection() throws SQLException {
        if (connection == null) {
            synchronized (ConnectionManager.class) {
                if (connection == null) {
                    connection = DriverManager.getConnection(url, user, password);
                }
            }
        }
        return connection;
    }
}
