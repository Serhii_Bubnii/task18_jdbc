package com.bubnii.model.transformer;

import com.bubnii.model.annotation.Column;
import com.bubnii.model.annotation.ForeignKey;
import com.bubnii.model.annotation.PrimaryKeyComposite;
import com.bubnii.model.annotation.Table;
import com.bubnii.model.entity.CountryLanguage;
import com.bubnii.model.entity.FKCountryLanguage;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Transformer<T> {
    private final Class<T> clazz;

    public Transformer(Class<T> clazz) {
        this.clazz = clazz;
    }

    public Object fromResultSetToEntity(ResultSet resultSet) throws SQLException {
        Object entity = null;
        try {
            entity = clazz.getConstructor().newInstance();
            if (clazz.isAnnotationPresent(Table.class)) {
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    if (field.isAnnotationPresent(Column.class)) {
                        Column column = (Column) field.getAnnotation(Column.class);
                        String name = column.name();
                        int length = column.length();
                        field.setAccessible(true);
                        Class fileType = field.getType();
                        if (fileType == String.class) {
                            field.set(entity, resultSet.getString(name));
                        } else if (fileType == Integer.class) {
                            field.set(entity, resultSet.getInt(name));
                        } else if (fileType == Long.class) {
                            field.set(entity, resultSet.getLong(name));
                        } else if (fileType == Double.class) {
                            field.set(entity, resultSet.getDouble(name));
                        } else if (fileType == Float.class) {
                            field.set(entity, resultSet.getFloat(name));
                        }
                    } else if (field.isAnnotationPresent(PrimaryKeyComposite.class)) {
                        field.setAccessible(true);
                        Class fieldType = field.getType();
                        Object FK = fieldType.getConstructor().newInstance();
                        field.set(entity, FK);
                        Field[] fieldsInner = fieldType.getDeclaredFields();
                        for (Field fieldInner : fieldsInner) {
                            if (fieldInner.isAnnotationPresent(Column.class)) {
                                Column column = (Column) fieldInner.getAnnotation(Column.class);
                                String name = column.name();
                                int length = column.length();
                                fieldInner.setAccessible(true);
                                Class fieldInnerType = fieldInner.getType();
                                if (fieldInnerType == String.class) {
                                    fieldInner.set(FK, resultSet.getString(name));
                                } else if (fieldInnerType == Integer.class) {
                                    fieldInner.set(FK, resultSet.getInt(name));
                                }
                            }
                        }
                    } else if (field.isAnnotationPresent(ForeignKey.class)) {
                        field.setAccessible(true);
                        Class fieldType = field.getType();
                        Object FK = fieldType.getConstructor().newInstance();
                        field.set(entity, FK);
                        Field[] fieldsInner = fieldType.getDeclaredFields();
                        for (Field fieldInner : fieldsInner){
                            if (fieldInner.isAnnotationPresent(Column.class)){
                                Column column = (Column) fieldInner.getAnnotation(Column.class);
                                String name = column.name();
                                int length = column.length();
                                fieldInner.setAccessible(true);
                                Class fieldInnerType = fieldInner.getType();
                                if (fieldInnerType == FKCountryLanguage.class) {
                                    fieldInner.set(FK, resultSet.getObject(name));
                                }
                            }
                        }
                    }
                }
            }
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        return entity;
    }
}
