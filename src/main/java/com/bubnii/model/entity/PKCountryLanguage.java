package com.bubnii.model.entity;

import com.bubnii.model.annotation.Column;

public class PKCountryLanguage {
    @Column(name = "CountryCode")
    private String countryCode;
    @Column(name = "Language")
    private String language;

    public PKCountryLanguage() {
    }

    public PKCountryLanguage(String countryCode, String language) {
        this.countryCode = countryCode;
        this.language = language;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "PKCountryLanguage{" +
                "countryCode='" + countryCode + '\'' +
                ", language='" + language + '\'' +
                '}';
    }
}
