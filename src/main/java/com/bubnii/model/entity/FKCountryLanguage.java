package com.bubnii.model.entity;

import com.bubnii.model.annotation.ForeignKey;

public class FKCountryLanguage {
    @ForeignKey()
    private CountryLanguage countryCode;

    public FKCountryLanguage() {
    }

    public FKCountryLanguage(CountryLanguage countryCode) {
        this.countryCode = countryCode;
    }


    public CountryLanguage getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(CountryLanguage countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public String toString() {
        return "FKCountryLanguage{" +
                "countryCode=" + countryCode +
                '}';
    }
}
