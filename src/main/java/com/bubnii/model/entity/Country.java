package com.bubnii.model.entity;

import com.bubnii.model.annotation.*;

@Table(name = "country")
public class Country {
    @PrimaryKey
    @Column(name = "Code", length = 3)
    private  String code;
    @Column(name = "Name", length = 52)
    private String name;
    @Column(name = "Continent")
    private String continent;
    @Column(name = "Region", length = 26)
    private String region;
    @Column(name = "SurfaceArea")
    private Double surfaceArea;
    @Column(name = "IndepYear")
    private Integer indepYear;
    @Column(name = "Population")
    private Long population;
    @Column(name = "LifeExpectancy")
    private Double lifeExpectancy;
    @Column(name = "GNP")
    private Double GNP;
    @Column(name = "GNPOld")
    private Double GNPOld;
    @Column(name = "LocalName", length = 45)
    private String localName;
    @Column(name = "GovernmentForm",length = 45)
    private String governmentForm;
    @Column(name = "HeadOfState", length = 60)
    private String headOfState;
    @Column(name = "Capital")
    private Long capital;
    @Column(name = "Code2",length = 2)
    private String code2;
    @ForeignKey
    private FKCountryLanguage fkCountryLanguage;

    public Country() {}

    public Country(String code, String name, String continent, String region, Double surfaceArea,
                   Integer indepYear, Long population, Double lifeExpectancy, Double GNP,
                   Double GNPOld, String localName, String governmentForm, String headOfState,
                   Long capital, String code2, FKCountryLanguage fkCountryLanguage) {
        this.code = code;
        this.name = name;
        this.continent = continent;
        this.region = region;
        this.surfaceArea = surfaceArea;
        this.indepYear = indepYear;
        this.population = population;
        this.lifeExpectancy = lifeExpectancy;
        this.GNP = GNP;
        this.GNPOld = GNPOld;
        this.localName = localName;
        this.governmentForm = governmentForm;
        this.headOfState = headOfState;
        this.capital = capital;
        this.code2 = code2;
        this.fkCountryLanguage = fkCountryLanguage;
    }

    public Country(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Double getSurfaceArea() {
        return surfaceArea;
    }

    public void setSurfaceArea(Double surfaceArea) {
        this.surfaceArea = surfaceArea;
    }

    public Integer getIndepYear() {
        return indepYear;
    }

    public void setIndepYear(Integer indepYear) {
        this.indepYear = indepYear;
    }

    public Long getPopulation() {
        return population;
    }

    public void setPopulation(Long population) {
        this.population = population;
    }

    public Double getLifeExpectancy() {
        return lifeExpectancy;
    }

    public void setLifeExpectancy(Double lifeExpectancy) {
        this.lifeExpectancy = lifeExpectancy;
    }

    public Double getGNP() {
        return GNP;
    }

    public void setGNP(Double GNP) {
        this.GNP = GNP;
    }

    public Double getGNPOld() {
        return GNPOld;
    }

    public void setGNPOld(Double GNPOld) {
        this.GNPOld = GNPOld;
    }

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public String getGovernmentForm() {
        return governmentForm;
    }

    public void setGovernmentForm(String governmentForm) {
        this.governmentForm = governmentForm;
    }

    public String getHeadOfState() {
        return headOfState;
    }

    public void setHeadOfState(String headOfState) {
        this.headOfState = headOfState;
    }

    public Long getCapital() {
        return capital;
    }

    public void setCapital(Long capital) {
        this.capital = capital;
    }

    public String getCode2() {
        return code2;
    }

    public void setCode2(String code2) {
        this.code2 = code2;
    }

    public FKCountryLanguage getFkCountryLanguage() {
        return fkCountryLanguage;
    }

    public void setFkCountryLanguage(FKCountryLanguage fkCountryLanguage) {
        this.fkCountryLanguage = fkCountryLanguage;
    }

    @Override
    public String toString() {
        return "Country{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", continent='" + continent + '\'' +
                ", region='" + region + '\'' +
                ", surfaceArea=" + surfaceArea +
                ", indepYear=" + indepYear +
                ", population=" + population +
                ", lifeExpectancy=" + lifeExpectancy +
                ", GNP=" + GNP +
                ", GNPOld=" + GNPOld +
                ", localName='" + localName + '\'' +
                ", governmentForm='" + governmentForm + '\'' +
                ", headOfState='" + headOfState + '\'' +
                ", capital=" + capital +
                ", code2='" + code2 + '\'' +
                ", fkCountryLanguage=" + fkCountryLanguage +
                '}';
    }
}
