package com.bubnii.model.entity;

import com.bubnii.model.annotation.Column;
import com.bubnii.model.annotation.PrimaryKeyComposite;
import com.bubnii.model.annotation.Table;

@Table(name = "countrylanguage")
public class CountryLanguage {
    @PrimaryKeyComposite
    private PKCountryLanguage pk;
    @Column(name = "IsOfficial")
    private String isOfficial;
    @Column(name = "Percentage")
    private Double percentage;

    public CountryLanguage() {
    }

    public CountryLanguage(PKCountryLanguage pk, String isOfficial, Double percentage) {
        this.pk = pk;
        this.isOfficial = isOfficial;
        this.percentage = percentage;
    }

    public PKCountryLanguage getPk() {
        return pk;
    }

    public void setPk(PKCountryLanguage pk) {
        this.pk = pk;
    }

    public String getOfficial() {
        return isOfficial;
    }

    public void setOfficial(String official) {
        isOfficial = official;
    }

    public Double getPercentage() {
        return percentage;
    }

    public void setPercentage(Double percentage) {
        this.percentage = percentage;
    }

    @Override
    public String toString() {
        return "CountryLanguage{" +
                "pk=" + pk +
                ", isOfficial=" + isOfficial +
                ", percentage=" + percentage +
                '}';
    }
}
