package com.bubnii.model.entity;

import com.bubnii.model.annotation.Column;
import com.bubnii.model.annotation.PrimaryKey;
import com.bubnii.model.annotation.Table;

@Table(name = "city")
public class City {
    @PrimaryKey
    @Column(name = "ID")
    private Integer id;
    @Column(name = "Name", length = 35)
    private String name;
    @Column(name = "CountryCode", length = 3)
    private String countryCode;
    @Column(name = "District", length = 20)
    private String district;
    @Column(name = "Population")
    private Long population;

    public City() {
    }

    public City(Integer id, String name, String countryCode, String district, Long population) {
        this.id = id;
        this.name = name;
        this.countryCode = countryCode;
        this.district = district;
        this.population = population;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public Long getPopulation() {
        return population;
    }

    public void setPopulation(Long population) {
        this.population = population;
    }

    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", district='" + district + '\'' +
                ", population=" + population +
                '}';
    }
}
