package com.bubnii.model.DAO;

import com.bubnii.model.entity.City;

public interface CityDao extends GeneralDao<City, Integer> {

}
