package com.bubnii.model.DAO;

import java.sql.SQLException;
import java.util.List;

public interface GeneralDao<T, ID> {
    List<T> selectAll() throws SQLException;

    T selectById(ID id) throws SQLException;

    int deleteById(ID id) throws SQLException;

    int insert(T entity) throws SQLException;

    int update(T entity) throws SQLException;
}
