package com.bubnii.model.DAO;

import com.bubnii.model.entity.CountryLanguage;
import com.bubnii.model.entity.PKCountryLanguage;

public interface CountryLanguageDao extends GeneralDao<CountryLanguage, PKCountryLanguage> {
}
