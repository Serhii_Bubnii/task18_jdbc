package com.bubnii.model.DAO.implementation;

import com.bubnii.model.DAO.CountryLanguageDao;
import com.bubnii.model.connection.ConnectionManager;
import com.bubnii.model.entity.CountryLanguage;
import com.bubnii.model.entity.PKCountryLanguage;
import com.bubnii.model.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class CountryLanguageDaoImpl implements CountryLanguageDao {

    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("sql_queries");

    @Override
    public List<CountryLanguage> selectAll() throws SQLException {
        String SELECT_ALL = resourceBundle.getString("SELECT_ALL_FROM_LANGUAGE");
        List<CountryLanguage> countryLanguages = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SELECT_ALL)) {
                while (resultSet.next()) {
                    countryLanguages.add((CountryLanguage) new Transformer(CountryLanguage.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return countryLanguages;
    }

    @Override
    public CountryLanguage selectById(PKCountryLanguage pkCountryLanguage) throws SQLException {
        String SELECT_BY_ID = resourceBundle.getString("SELECT_BY_ID_LANGUAGE");
        CountryLanguage language = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID)) {
            preparedStatement.setString(1, pkCountryLanguage.getCountryCode());
            preparedStatement.setString(2, pkCountryLanguage.getLanguage());
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    language = (CountryLanguage) new Transformer(CountryLanguage.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return language;
    }

    @Override
    public int deleteById(PKCountryLanguage pkCountryLanguage) throws SQLException {
        String DELETE_LANGUAGE = resourceBundle.getString("DELETE_LANGUAGE");
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_LANGUAGE)) {
            preparedStatement.setString(1, pkCountryLanguage.getCountryCode());
            preparedStatement.setString(2, pkCountryLanguage.getLanguage());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int insert(CountryLanguage entity) throws SQLException {
        String INSERT = resourceBundle.getString("INSERT_LANGUAGE");
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
            preparedStatement.setString(1, entity.getPk().getCountryCode());
            preparedStatement.setString(2, entity.getPk().getLanguage());
            preparedStatement.setString(3, entity.getOfficial());
            preparedStatement.setDouble(4, entity.getPercentage());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(CountryLanguage entity) throws SQLException {
        String UPDATE = resourceBundle.getString("UPDATE_LANGUAGE");
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = conn.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, entity.getOfficial());
            preparedStatement.setDouble(2, entity.getPercentage());
            preparedStatement.setString(3, entity.getPk().getCountryCode());
            preparedStatement.setString(4, entity.getPk().getLanguage());
            return preparedStatement.executeUpdate();
        }
    }
}
