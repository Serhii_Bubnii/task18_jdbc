package com.bubnii.model.DAO.implementation;

import com.bubnii.model.DAO.CountryDao;
import com.bubnii.model.connection.ConnectionManager;
import com.bubnii.model.entity.Country;
import com.bubnii.model.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class CountryDaoImpl implements CountryDao {

    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("sql_queries");

    @Override
    public List<Country> selectAll() throws SQLException {
        String SELECT_ALL = resourceBundle.getString("SELECT_ALL_FROM_COUNTRY");
        List<Country> countries = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SELECT_ALL)) {
                while (resultSet.next()) {
                    countries.add((Country) new Transformer(Country.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return countries;
    }

    @Override
    public Country selectById(String code) throws SQLException {
        String SELECT_BY_ID = resourceBundle.getString("SELECT_BY_ID_COUNTRY");
        Country country = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID)) {
            preparedStatement.setString(1, code);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    country = (Country) new Transformer(Country.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return country;
    }

    @Override
    public int deleteById(String code) throws SQLException {
        String DELETE_COUNTRY = resourceBundle.getString("DELETE_COUNTRY");
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_COUNTRY)) {
            preparedStatement.setString(1, code);
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int insert(Country entity) throws SQLException {
        String INSERT = resourceBundle.getString("INSERT_COUNTRY");
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
            preparedStatement.setString(1, entity.getCode());
            preparedStatement.setString(2, entity.getName());
            preparedStatement.setString(3, entity.getContinent());
            preparedStatement.setString(4, entity.getRegion());
            preparedStatement.setDouble(5, entity.getSurfaceArea());
            preparedStatement.setInt(6, entity.getIndepYear());
            preparedStatement.setLong(7, entity.getPopulation());
            preparedStatement.setDouble(8, entity.getLifeExpectancy());
            preparedStatement.setDouble(9, entity.getGNP());
            preparedStatement.setDouble(10, entity.getGNPOld());
            preparedStatement.setString(11, entity.getLocalName());
            preparedStatement.setString(12, entity.getGovernmentForm());
            preparedStatement.setString(13, entity.getHeadOfState());
            preparedStatement.setLong(14, entity.getCapital());
            preparedStatement.setString(15, entity.getCode2());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Country entity) throws SQLException {
        String UPDATE = resourceBundle.getString("UPDATE_COUNTRY");
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setString(2, entity.getContinent());
            preparedStatement.setString(3, entity.getRegion());
            preparedStatement.setDouble(4, entity.getSurfaceArea());
            preparedStatement.setInt(5, entity.getIndepYear());
            preparedStatement.setLong(6, entity.getPopulation());
            preparedStatement.setDouble(7, entity.getLifeExpectancy());
            preparedStatement.setDouble(8, entity.getGNP());
            preparedStatement.setDouble(9, entity.getGNPOld());
            preparedStatement.setString(10, entity.getLocalName());
            preparedStatement.setString(11, entity.getGovernmentForm());
            preparedStatement.setString(12, entity.getHeadOfState());
            preparedStatement.setLong(13, entity.getCapital());
            preparedStatement.setString(14, entity.getCode2());
            preparedStatement.setString(15, entity.getCode());
            return preparedStatement.executeUpdate();
        }
    }
}
