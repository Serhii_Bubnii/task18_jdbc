package com.bubnii.model.DAO.implementation;

import com.bubnii.model.DAO.CityDao;
import com.bubnii.model.connection.ConnectionManager;
import com.bubnii.model.entity.City;
import com.bubnii.model.transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class CityDaoImpl implements CityDao {

    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("sql_queries");

    @Override
    public List<City> selectAll() throws SQLException {
        String SELECT_ALL = resourceBundle.getString("SELECT_ALL_FROM_CITY");
        List<City> cities = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SELECT_ALL)) {
                while (resultSet.next()) {
                    cities.add((City) new Transformer(City.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return cities;
    }

    @Override
    public City selectById(Integer id) throws SQLException {
        String SELECT_BY_ID = resourceBundle.getString("SELECT_BY_ID_CITY");
        City city = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID)) {
            preparedStatement.setInt(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    city = (City) new Transformer(City.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return city;
    }

    @Override
    public int deleteById(Integer id) throws SQLException {
        String DELETE_CITY = resourceBundle.getString("DELETE_CITY");
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_CITY)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int insert(City city) throws SQLException {
        String INSERT = resourceBundle.getString("INSERT_CITY");
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
            preparedStatement.setInt(1, city.getId());
            preparedStatement.setString(2, city.getName());
            preparedStatement.setString(3, city.getCountryCode());
            preparedStatement.setString(4, city.getDistrict());
            preparedStatement.setLong(5, city.getPopulation());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(City city) throws SQLException {
        String UPDATE = resourceBundle.getString("UPDATE_CITY");
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = conn.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, city.getName());
            preparedStatement.setString(2, city.getCountryCode());
            preparedStatement.setString(3, city.getDistrict());
            preparedStatement.setLong(4, city.getPopulation());
            preparedStatement.setInt(5, city.getId());
            return preparedStatement.executeUpdate();
        }
    }
}
