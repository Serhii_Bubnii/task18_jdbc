package com.bubnii.model.DAO;

import com.bubnii.model.entity.Country;

public interface CountryDao extends GeneralDao<Country, String> {

}
